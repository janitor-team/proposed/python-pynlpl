python-pynlpl (1.2.9-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Standards-Version: 4.4.1
  * debhelper compatibility level 12: debhelper-compat (= 12)
  * drop python-pynlpl (remove python2). Closes: #938080.
  * update Vcs tags to salsa.debian.org
  * Build-Depends: python3-httplib2 (>= 0.6),
     python3-lxml (>= 2.2),
     python3-numpy
  * add debian patch skip_folia_tests.patch to skip FoLiA tests (folia
    and FQN).  example.xml is not available for them.
  * debian/tests Restrictions: allow-stderr (to ignore deprecation
    warning about pynlpl.formats.folia)

 -- Drew Parsons <dparsons@debian.org>  Sun, 20 Oct 2019 22:12:14 +0800

python-pynlpl (1.1.2-1) unstable; urgency=medium

  * New upstream release (1.1.2)

 -- Maarten van Gompel <proycon@anaproy.nl>  Mon, 02 Jan 2017 12:22:00 +0200

python-pynlpl (1.0.9-1) unstable; urgency=medium

  * New upstream release (1.0.9)
  * debian/tests: fixed
  * debian/compat: changed to 10
  * debian/control: cleanup (whitespace, ordering, descriptions)
  * manpages/pynlpl-computepmi.1: spelling fix
  * debian/copyright: fixed order
   (thanks to Mattia Rizzolo for pointing out most of the above)

 -- Maarten van Gompel <proycon@anaproy.nl>  Tue, 18 Oct 2016 12:22:00 +0200

python-pynlpl (1.0.5-1) unstable; urgency=medium

  * New upstream release (1.0.5)
  * debian/control: Added VCS fields, applied cme fix dpkg
  * debian/rules: removed unneeded commented lines
  * debian/manpages: moved manpages here
  * debian/copyright: Applied cme fix dpkg
    (thanks to Anton Gladky for pointing these four out)
  * debian/control: Removed Joost van Baal-Ilić as uploader
  * Added autopkgtest

 -- Maarten van Gompel <proycon@anaproy.nl>  Sun, 04 Sep 2016 22:54:00 +0200

python-pynlpl (0.7.7.1-2) unstable; urgency=medium

  * Removed texinfo manual reference
  * debian/copyright: All GPL-3 now 
   (thanks to Thorsten Alteholz for pointing it out)
   also added comments for MIT work.

 -- Maarten van Gompel <proycon@anaproy.nl>  Thu, 11 Feb 2016 10:17:33 +0100

python-pynlpl (0.7.7.1-1) unstable; urgency=low

  * Initial release. Closes: #771592 

 -- Maarten van Gompel <proycon@anaproy.nl>  Fri, 20 Nov 2015 22:46:15 +0100
